pipeline {
    agent any

    options {
        buildDiscarder(logRotator(numToKeepStr: '10'))
        disableConcurrentBuilds()
    }

    stages {
        stage('Install') {
            steps {
                sh 'yarn install'
            }
        }
        stage('Build') {
            parallel {
                stage('Bundle') {
                    steps {
                        sh 'yarn build/bundle'
                    }
                }
                stage('Webcomponents') {
                    steps {
                        sh 'yarn build/header'
                        sh 'yarn build/footer'
                        sh 'yarn build/me'
                    }
                }
            }
        }
        stage('Deploy') {
            steps {
                sshagent(['ryzom']) {
                    sh 'ssh ryzom@ryci.vendale.net rm -rf /var/www/ryci/*'
                    sh 'scp -r ./dist/* ryzom@ryci.vendale.net:/var/www/ryci'
                    sh 'ssh ryzom@ryci.vendale.net chmod -R 777 /var/www/ryci/*'
                }
            }
        }
    }
}
