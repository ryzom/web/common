# ryzom-common-ci

RyzomCI is home to a common style, header and footer which are intended to be integrated on all of Ryzom's websites.

RyzomCI provides a Bootstrap 4 Theme already configured. For more information on how to use Bootstrap please refer to [the Bootstrap website](https://getbootstrap.com/docs/4.3/getting-started/introduction/).

## usage as node_modules dependency

add the following line to the dependecies in your package.json
> "ryzom-web-common": "https://gitlab.com/ryzom/web/common.git#dev"

the following assets are available as imports then:
* scss
* images
* header and footer Vue.js components

## usage as an external dependency

RyzomCI offers three compiled and ready to use resources:
* bundle: corporate identity css
* webcomponent: header with menu
* webcomponent: footer

### CSS Bootstrap Theme

To use the RyzomCI bootstrap theme, just include this line in the ```<head>``` section of the website:

```html
<script src="https://ci.ryzom.com/bundles/common.bundle.js"></script>
```

### The Header Webcomponent

Include this in the ```<head>``` section of your website:
```html
<script src="https://unpkg.com/vue"></script>
<script src="https://ci.ryzom.com/webcomponents/header/ryzom-header.min.js"></script>
```
> Note: if you are using both, header and footer, you only need to include "vue" once. 

And the include this right after opening the ```<body>``` tag:
```html
<ryzom-header></ryzom-header>
```

#### Configuring the header

The following options are available:

*
*
*
*
*
*
*


### The Footer Webcomponent

Include this in the ```<head>``` section of your website:
```html
<script src="https://unpkg.com/vue"></script>
<script src="https://ci.ryzom.com/webcomponents/footer/ryzom-footer.min.js"></script>
```
> Note: if you are using both, header and footer, you only need to include "vue" once. 

And the include this before closing the ```<body>``` tag:
```html
<ryzom-footer></ryzom-footer>
```

#### Configuring the footer

The following options are available:

* lang
* tos-link
* privacy-link
* use-events (see #using-events-instead-of-links)

An example:
```html
<ryzom-footer lang="en" tos-link="https://terms-of-service-url" privacy-link="https://privacy-info-page"></ryzom-footer>
```
